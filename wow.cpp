#include <bits/stdc++.h>
using namespace std;

string is_diff_whitespace(string str){
	string result;
	for (auto it:str){
		if (it!=' ')
			result+=it;
	}
	return result;
}
int main(int argc,  char** argv){
	ifstream f("finalFile");
	string str;
	map<string,vector<string> > meds;

	while (getline(f,str)){
		if (str.find("name:")!=std::string::npos){
			string name;
			getline(f,name);
			while (getline(f,str)){
				if (str.find("-1111")!=std::string::npos) break;
				meds[name].push_back(str);
			}
		}
	}

	map<string,vector<string> > final_list;




	for(auto it:meds){

		final_list[is_diff_whitespace(it.first)]=vector<string>();
		for(auto it2:it.second){
			if (is_diff_whitespace(it2)!=""){
				final_list[is_diff_whitespace(it.first)].push_back(is_diff_whitespace(it2));
			}
		}
	}

//	for(auto it:final_list){
//		cout<<"p"<<it.first<<"p"<<'\n';
//	}

	
	meds=final_list;
	vector<string> to_check;

	for(int i=1;i<argc;++i){
		string now_med=string(argv[i]);
		now_med=is_diff_whitespace(now_med);
		if (meds.find(now_med)==meds.end()){

			cout<<"Invalid name of medicine, not found in the database "<<now_med<<'\n';
			break;		
		}
		to_check.push_back(now_med);
	}
	for(auto it:meds["Sargramostim"])
		cout<<it<<'\n';

	for(int i=0;i<to_check.size();++i)
	{
		for(int j=0;j<to_check.size();++j){
			if (i!=j){
				if (std::find(meds[to_check[j]].begin(),meds[to_check[j]].end(),to_check[i])!=meds[to_check[j]].end()){
					cout<<"possible Drug interraction between "<<to_check[i]<<" "<<to_check[j]<<'\n';
				}
			}
		}
	}
	return 0;
}